This folder is an archive for the agents developed and submitted in the competition.

The final leaderboard of agents, averaged over the [100 seeds provided](https://gitlab.com/rangl-public/generation-scheduling-challenge-january-2021/-/blob/master/local_agent_training_and_evaluation/seeds.csv), was:

1.    zeepkist-RL v1.1.0: 1052
2.    AlphaGenerationScheduling: 1069
3.    zeepkist-MPC v1.0.4: 1070
4.    escjc_v02: 1118
5.    nextgenerators-babylearner v93.7.1: 1257
6.    nextgenerators-oldschool v1: 1278
7.    jmontalvo: 1684
8.    psjw2-rl v1.0.0: 2383

The final leaderboard of teams at the [RangL competition platform](http://challenge1-rangl.uksouth.cloudapp.azure.com:8888) (which was much noisier, due at least to the use of a single seed) was:

1.	zeepkist (zeepkist_mpc v1.1.0) : 1,123
2.	escjc (escjc_v02): 1,292
3.	energy_arg: 1,725
4.	AlphaGenerationScheduling: 1,861
5.	jmontalvo: 2,093
6.	psjw2: 2,725

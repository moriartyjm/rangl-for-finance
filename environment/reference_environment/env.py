import math

import pandas as pd
import numpy as np
import gym
from gym import spaces, logger
from gym.utils import seeding
import matplotlib.pyplot as plt


class Parameters:
    # (Avoid sampling random variables here: they would not be resampled upon reset())
    # problem-specific parameters
    # imbalance_cost_factor_high = 50
    # imbalance_cost_factor_low = 7
    # ramp_1_max = 0.2
    # ramp_2_max = 0.5
    # ramp_1_min = -0.2
    # ramp_2_min = -0.5
    # generator_1_max = 3
    # generator_1_min = 0.5
    # generator_2_max = 2
    # generator_2_min = 0.5
    # generator_1_cost = 1
    # generator_2_cost = 5

    # time parameters
    steps_per_episode = 96
    # first_peak_time = 5
    
    # portfolio specific parameters
    discount_factor_beta = np.ones(steps_per_episode)
    discount_factor_beta[-1] = 1.0 # final discount factor at time T; may be set to other values
    
    # log returns model parameters
    asset_1_drift_mean = 0.015
    asset_1_diffusion_std = 0.01
    asset_2_drift_mean = 0.0015
    asset_2_diffusion_std = 0.1


param = Parameters()  # parameters singleton


class State:
    def __init__(self, seed=None):
        np.random.seed(seed=seed)
        self.initialise_state()

    def reset(self):
        self.initialise_state()
        return self.to_observation()

    def initialise_state(self):
        # derived variables
        # self.generator_1_level = 1.5
        # self.generator_2_level = 0.5
        self.asset_1_price = 20.0
        self.asset_2_price = 100.0
        # self.assets_prices = np.array([20.0, 100.0])
        self.portfolio_action_initial = np.array([50000.0, 10000.0])
        # self.portfolio_valuation = self.portfolio_action_initial @ self.assets_prices
        self.portfolio_valuation = self.portfolio_action_initial[0] * self.asset_1_price + self.portfolio_action_initial[1] * self.asset_2_price
        self.consumption = 0.0
        
        # forecast variables
        # initialise all predictions to 2
        # self.prediction_array = np.full(param.steps_per_episode, 2)
        # # sample the time of the second peak
        # self.second_peak_time = np.random.randint(low=10, high=95)
        # # add the peak demands
        # self.prediction_array[param.first_peak_time] = 4
        # self.prediction_array[self.second_peak_time] = 8
        
        # self.agent_prediction = self.prediction_array

        # time variables
        # NOTE: our convention is to update step_count at the beginning of the gym step() function
        self.step_count = -1
        
        # histories
        self.observations_all = []
        self.actions_all = []
        self.rewards_all = []
        # self.agent_predictions_all = []
        # self.generator_1_levels_all = []
        # self.generator_2_levels_all = []
        self.asset_1_prices_all = []
        self.asset_2_prices_all = []
        # self.assets_prices_all = []
        self.portfolio_valuations_all = []
        self.consumptions_all = []

    def to_observation(self):
        # observation = (
        #     self.step_count,
        #     self.generator_1_level,
        #     self.generator_2_level,
        # ) + tuple (self.agent_prediction)
        observation = (
            self.step_count,
            self.asset_1_price,
            self.asset_2_price,
        ) # + tuple (np.ones(param.steps_per_episode)) # + tuple (self.agent_prediction)
        
        return observation

    def is_done(self):
        done = bool(
            self.step_count >= param.steps_per_episode - 1
            )
        return done

    # def set_agent_prediction(self):
    #     self.agent_prediction = self.prediction_array
        


def record(state, action, reward):
    state.observations_all.append(state.to_observation())
    state.actions_all.append(action)
    state.rewards_all.append(reward)
    # state.agent_predictions_all.append(state.agent_prediction)
    # state.generator_1_levels_all.append(state.generator_1_level)
    # state.generator_2_levels_all.append(state.generator_2_level)
    state.asset_1_prices_all.append(state.asset_1_price)
    state.asset_2_prices_all.append(state.asset_2_price)
    state.portfolio_valuations_all.append(state.portfolio_valuation)
    state.consumptions_all.append(state.consumption)

def observation_space():
    # obs_low = np.full(99, -1000, dtype=np.float32) # last 96 entries of observation are the predictions
    # obs_low[0] = -1	# first entry of obervation is the timestep
    # obs_low[1] = 0.5	# min level of generator 1 
    # obs_low[2] = 0.5	# min level of generator 2
    # obs_high = np.full(99, 1000, dtype=np.float32) # last 96 entries of observation are the predictions
    # obs_high[0] = param.steps_per_episode	# first entry of obervation is the timestep
    # obs_high[1] = 3	# max level of generator 1 
    # obs_high[2] = 2	# max level of generator 2
    obs_low = np.full(3, 0, dtype=np.float32) # last 96 entries of observation are the predictions
    obs_low[0] = -1	# first entry of obervation is the timestep
    # obs_low[1] = 0.5	# min price of asset 1 
    # obs_low[2] = 0.5	# min price of asset 2
    obs_high = np.full(3, 1e6, dtype=np.float32) # last 96 entries of observation are the predictions
    obs_high[0] = param.steps_per_episode	# first entry of obervation is the timestep
    # obs_high[1] = 3	# max price of asset 1 
    # obs_high[2] = 2	# max price of asset 2
    result = spaces.Box(obs_low, obs_high, dtype=np.float32)
    return result


def action_space(state):
    # act_low = np.array(
    #     [
    #         0,    # lower bound of 1st action for sampling 
    #         0,    # lower bound of 2nd action for sampling 
    #     ],
    #     dtype=np.float32,
    # )
    # act_high = np.array(
    #     [
    #         3,    # upper bound of 1st action for sampling
    #         2,    # upper bound of 2nd action for sampling 
    #     ],
    #     dtype=np.float32,
    # )
    act_low = np.array(
        [
            0,    # lower bound of portfolio action on asset 1 for sampling 
            0,    # lower bound of portfolio action on asset 2 for sampling 
        ],
        dtype=np.float32,
    )
    act_high = np.array(
        [
            state.portfolio_valuation,    # upper bound of portfolio action on asset 1 for sampling 
            # this is assuming that the lowest price of all assets is 1, and the maximum will be the
            # total portfolio valuation divided by 1.
            state.portfolio_valuation,    # upper bound of portfolio action on asset 2 for sampling 
        ],
        dtype=np.float32,
    )
    result = spaces.Box(act_low, act_high, dtype=np.float32)
    return result


def apply_action(action, state):

    # # implement the generation levels requested by the agent 
    # state.generator_1_level = action[0]
    # state.generator_2_level = action[1]
    
    # # check the previous generation levels
    # if state.step_count == 0:
    #     state.generator_1_previous = 1.5
    #     state.generator_2_previous = 0.5
    # else:    
    #     state.generator_1_previous = state.generator_1_levels_all[state.step_count - 1]
    #     state.generator_2_previous = state.generator_2_levels_all[state.step_count - 1]

    # # calculate ramp rates 
    # generator_1_ramp = state.generator_1_level - state.generator_1_previous
    # generator_2_ramp = state.generator_2_level - state.generator_2_previous

    # # curtail the actions if they exceed the ramp rate constraints
    # if generator_1_ramp > param.ramp_1_max:
    #     state.generator_1_level = state.generator_1_previous + param.ramp_1_max
    # if generator_1_ramp < param.ramp_1_min:
    #     state.generator_1_level = state.generator_1_previous + param.ramp_1_min
    # if generator_2_ramp > param.ramp_2_max:
    #     state.generator_2_level = state.generator_2_previous + param.ramp_2_max
    # if generator_2_ramp < param.ramp_2_min:
    #     state.generator_2_level = state.generator_2_previous + param.ramp_2_min

    # # curtail the actions if they exceed the generator level constraints
    # if state.generator_1_level > param.generator_1_max:
    #     state.generator_1_level = param.generator_1_max
    # if state.generator_1_level < param.generator_1_min:
    #     state.generator_1_level = param.generator_1_min
    # if state.generator_2_level > param.generator_2_max:
    #     state.generator_2_level = param.generator_2_max
    # if state.generator_2_level < param.generator_2_min:
    #     state.generator_2_level = param.generator_2_min
    
    
    # sanity check using fixed action: allocate 40% of the total wealth to asset 1 and 40% to asset 2, and consume the rest 20%
    # action[0] = state.portfolio_valuation*0.4/state.asset_1_price
    # action[1] = state.portfolio_valuation*0.4/state.asset_2_price
    
    # calculate the proposed portfolio valuation due to new action
    state.proposed_portfolio_valuation = action[0] * state.asset_1_price + action[1] * state.asset_2_price
    # check if this proposed portfolio is implementable given the total wealth/current portfolio valuation,
    # and if out of bound/not implementable, reset the action proportionally (i.e., reallocate the total 
    # wealth to the 2 assets according to the ratio of action[0] to action[1]), and set consumption to 0; 
    # OTOH, if implementable, calculate the consumption
    if  state.proposed_portfolio_valuation > state.portfolio_valuation:
        action[0] = action[0] * state.portfolio_valuation/state.proposed_portfolio_valuation
        action[1] = action[1] * state.portfolio_valuation/state.proposed_portfolio_valuation
        state.consumption = 0
    else:
        state.consumption = state.portfolio_valuation - state.proposed_portfolio_valuation
    
    if state.step_count != param.steps_per_episode - 1:
        # if not at the last step to apply an action, then simulate the new prices according to a log returns model
        state.asset_1_price = state.asset_1_price * np.exp(param.asset_1_drift_mean + param.asset_1_diffusion_std * np.random.randn())
        state.asset_2_price = state.asset_2_price * np.exp(param.asset_2_drift_mean + param.asset_2_diffusion_std * np.random.randn())
        
    # calculate new valuation of portfolio due to the new prices using the implementable action
    # this also applies to the final step, where the prices will not be updated by simulation
    state.portfolio_valuation = action[0] * state.asset_1_price + action[1] * state.asset_2_price

    return state


def to_reward(state, done):
    # imbalance = total generation level - current demand level
    # imbalance = state.generator_1_level + state.generator_2_level - state.prediction_array[state.step_count]
    # if imbalance > 0:
    #     imbalance_cost = imbalance * param.imbalance_cost_factor_low
    # else:
    #     imbalance_cost = -1 * imbalance * param.imbalance_cost_factor_high
    # fuel_cost = param.generator_1_cost * state.generator_1_level + param.generator_2_cost * state.generator_2_level
    # reward = 0 - imbalance_cost - fuel_cost
    
    # uncomment for Warmup phase:
    # if state.step_count != 1:
    #     reward = 0 
    
    # uncomment for Peak phase:
    # if state.step_count != 5:
    #     reward = 0
    
    if state.step_count != param.steps_per_episode - 1:
        reward = param.discount_factor_beta[state.step_count] * state.consumption
    else:
        reward = param.discount_factor_beta[state.step_count] * state.portfolio_valuation
    
    return reward


# def update_prediction_array(prediction_array):
#     prediction_array = prediction_array + 0.1 * np.random.randn(1,len(prediction_array))[0]
#     return prediction_array


def plot_episode(state, fname):
    fig, ax = plt.subplots(2, 2)

    # cumulative total cost
    plt.subplot(221)
    plt.plot(np.cumsum(state.rewards_all))
    plt.xlabel("time")
    plt.ylabel("cumulative reward")
    plt.tight_layout()
    # could be expanded to include individual components of the reward

    # # generator levels
    # plt.subplot(222)
    # plt.plot(np.array(state.generator_1_levels_all))
    # plt.plot(np.array(state.generator_2_levels_all))
    # plt.xlabel("time")
    # plt.ylabel("generator levels")
    # plt.tight_layout()
    
    # asset prices
    plt.subplot(222)
    plt.plot(np.array(state.asset_1_prices_all))
    plt.plot(np.array(state.asset_2_prices_all))
    plt.xlabel("time")
    plt.ylabel("asset prices")
    plt.tight_layout()

    # actions
    plt.subplot(223)
    plt.plot(np.array(state.actions_all))
    plt.xlabel("time")
    plt.ylabel("portfolio holding actions")
    plt.tight_layout()


    # # agent predictions
    # plt.subplot(224)
    # plt.plot(np.array(state.agent_predictions_all))
    # plt.xlabel("time")
    # plt.ylabel("predictions")
    # plt.tight_layout()
    
    # portfolio valuation
    plt.subplot(224)
    plt.plot(np.array(state.portfolio_valuations_all))
    plt.xlabel("time")
    plt.ylabel("portfolio valuations")
    plt.tight_layout()

    plt.savefig(fname)

def score(state):
    value1 = np.sum(state.rewards_all)
    return {"value1" : value1}


# TODO
# def fetch_prediction(prediction_array, step):
#     prediction = prediction_array[step]
#     return prediction


class GymEnv(gym.Env):
    def __init__(self):
        self.seed()
        self.initialise_state()

    def initialise_state(self):
        self.state = State(seed=self.current_seed)
        # self.action_space = action_space()
        self.action_space = action_space(self.state)
        self.observation_space = observation_space()
        self.param = param

    def reset(self):
        self.initialise_state()
        observation = self.state.to_observation()
        return observation

    def step(self, action):
        self.state.step_count += 1
        # self.state.prediction_array = update_prediction_array(
        #     self.state.prediction_array
        # )
        self.state = apply_action(action, self.state)
        # self.state.set_agent_prediction()
        observation = self.state.to_observation()
        done = self.state.is_done()
        reward = to_reward(self.state, done)
        record(self.state, action, reward)
        return observation, reward, done, {}

    def seed(self, seed=None):
        self.current_seed = seed

    def score(self):
        if self.state.is_done():
            return score(self.state)
        else:
            return None
        #print('the score to be returned is: ',score(self.state))
        #return score(self.state)

    def plot(self, fname="episode.png"):
        plot_episode(self.state, fname)

    def render(self):
        pass

    def close(self):
        pass

